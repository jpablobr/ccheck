# -*- encoding: utf-8 -*-
module CCheck
  autoload :CLI,  'ccheck/cli'
  autoload :Card, 'ccheck/card'
  autoload :Luhn, 'ccheck/luhn'

  extend self

  # Public: Verifies if number is a valid CC number according to
  # Luhn algorithm.
  #
  # number - String
  #
  # Return Boolean
  def valid? number
    Luhn.new(number).valid?
  end

  # Public: Determines CC type.
  #
  # number - String
  #
  # Return String of CC type (Visa, MC). if not found "Unknown"
  def type number
    Card.new(number).type
  end

  # Public: Prints to stdout CC info.
  #
  # Input - String or Array
  #
  # If parameter is a String, prints CC info.
  # if parameter is an Array, print their info one by one.
  #
  def print input
    if input.is_a? Array
      input.each { |number| puts Card.new(number).to_s }
    else
      puts Card.new(input).to_s
    end
  end
end
