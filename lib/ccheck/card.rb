# -*- encoding: utf-8 -*-
module CCheck
  class Card
    attr_reader :number

    # Cleans the given raw CC number.
    #
    # raw_number - CC raw String
    #
    def initialize raw_number
      @number = cleanup(raw_number)
    end

    # Public: Determines CC type/brand.
    #
    # Returns CC type as a String or "Unknown" if not found.
    def type
      case number
      when /^3[47]\d{13}$/          ; return "AMEX"
      when /^4(\d{15}|\d{12})$/     ; return "VISA"
      when /^5\d{15}|36\d{14}$/     ; return "MasterCard"
      when /^6011\d{12}|650\d{13}$/ ; return "Discover"
      else                            return "Unknown"
      end
    end

    # Public: Formats CC information as such:
    # TYPE: NUMBERS (VALIDITY)
    #
    # Returns CC information as a String
    def to_s
      "%-28s (%s)\n" % [
        "#{type}: #{number}",
        Luhn.new(number).valid? ? 'valid' : 'invalid'
      ]
    end

    private

    # Private: Cleans up the given CC number
    #
    # raw_number - as String
    #
    # Returns CC number as String
    def cleanup raw_number
      raw_number.gsub(/\D/, '')
    end
  end
end
