# -*- encoding: utf-8 -*-
module CCheck
  class CLI
    require 'optparse'

    # Public: Parses user CLI input and sends to CCheck#print the CCs.
    #
    # argv - ARGV Array
    #
    def parse_options argv
      opts = OptionParser.new do |o|
        o.banner = "Usage: ccheck [-v] [-h] [CARD]"
        o.separator ""
        o.on("-h", "--help", "Print this help.") { $stderr.puts(opts) }
        o.on("-v", "--version", "Print version.") { return $stderr.puts(VERSION) }
        o.separator ""
        o.separator "Examples:"
        o.separator "\$ ccheck 4111111111111111"
        o.separator "\$ ccheck 4111111111111111 9111111111111111 '5105 1051 0510 5106'"
        o.separator "\$ cat cards-stdin.txt | while read card; do ccheck \$card; done"
      end
      opts.parse!(argv) rescue return $stderr.puts(opts)
      CCheck.print(argv)
    end
  end
end
