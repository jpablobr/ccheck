# -*- encoding: utf-8 -*-
module CCheck
  class Luhn
    attr_reader :card, :digits

    # Sets @card and also builds its luhn @digits.
    #
    # card - CC String
    #
    def initialize card
      @card   = card
      @digits = luhn_digits
    end

    # Public: Verifies that the given number is a valid CC.
    # Basically the third step of the Luhn algorighthm.
    # If the result is a multiple of 10, the number is valid.
    #
    # Returns Boolean
    def valid?
      digits.split('').inject(0) do |sum, digit|
        sum + digit.to_i
      end % 10 == 0
    end

    private

    # Private: Luhn's algorithm main logic.
    # It perfoms the first two steps.
    #
    # 1.  Starting with the next to last digit and continuing with
    #     every other digit going back to the beginning of the card,
    #     double the digit
    #
    # 2.  Sum all doubled and untouched digits in the number. For
    #     digits greater than 9 you will need to split them and sum
    #     the independently (i.e. `"10", 1 + 0`).
    #
    # Returns String
    def luhn_digits
      digits = ''
      card.reverse.chars.each_with_index do |digit,index|
        digits += digit if index % 2 == 0
        digits += (digit.to_i * 2).to_s if index % 2 == 1
      end
      digits
    end

  end
end
