# encoding: UTF-8
require File.expand_path('../lib/ccheck/version', __FILE__)

Gem::Specification.new do |s|
  s.name         = 'ccheck'
  s.homepage     = 'https://bitbucket.org/jpablobr/ccheck'
  s.summary      = 'A small implementation of the Luhn algorithm.'
  s.require_path = 'lib'
  s.authors      = ['Jose Pablo Barrantes']
  s.email        = ['xjpablobrx@gmail.com']
  s.version      = CCheck::Version
  s.platform     = Gem::Platform::RUBY
  s.files         = `git ls-files`.split("\n")
  s.executables   = `git ls-files`.split("\n").map{|f| f =~ /^bin\/(.*)/ ? $1 : nil}.compact
  s.require_path  = 'lib'

  s.add_development_dependency 'minitest', '~> 2.11'
end
