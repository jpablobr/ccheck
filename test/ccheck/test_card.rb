# -*- encoding: utf-8 -*-
require 'test_helper'
require 'minitest/autorun'

module CCheck
  class CardTest < MiniTest::Unit::TestCase

    def test_returns_the_correct_card_type
      [
        %w[VISA 4111111111111111],
        %w[VISA 4111111111111],
        %w[VISA 4012888888881881],
        %w[AMEX 378282246310005],
        %w[Discover 6011111111111117],
        %w[MasterCard 5105105105105100],
        %w[MasterCard 5105105105105106],
        %w[Unknown 9111111111111111]
      ].each { |card| assert_equal(card[0], Card.new(card[1]).type) }
    end

    def test_to_s_method_returns_properly_formatted_card
      assert_equal("VISA: 4111111111111111       (valid)\n",
             Card.new('4111111111111111').to_s)
      assert_equal("MasterCard: 5105105105105100 (valid)\n" ,
             Card.new('5105105105105100').to_s)
      assert_equal("MasterCard: 5105105105105106 (invalid)\n" ,
             Card.new('5105 1051 0510 5106').to_s)
    end
  end
end
