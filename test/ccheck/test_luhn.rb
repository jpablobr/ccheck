# -*- encoding: utf-8 -*-
require 'test_helper'
require 'minitest/autorun'

module CCheck
  class LuhnTest < MiniTest::Unit::TestCase

    def test_validates_card
      assert Luhn.new('4111111111111111').valid?    , 'Invalid card'
      refute Luhn.new('4111111111111').valid?       , 'valid card'
      assert Luhn.new('4012888888881881').valid?    , 'Invalid card'
      assert Luhn.new('378282246310005').valid?     , 'Invalid card'
      assert Luhn.new('6011111111111117').valid?    , 'Invalid card'
      assert Luhn.new('5105105105105100').valid?    , 'Invalid card'
      refute Luhn.new('5105 1051 0510 5106').valid? , 'valid card'
      refute Luhn.new('9111111111111111').valid?    , 'valid card'
    end
  end
end
