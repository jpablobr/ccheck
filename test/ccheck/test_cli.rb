# -*- encoding: utf-8 -*-
require 'test_helper'
require 'minitest/autorun'

module CCheck
  class CLITest < MiniTest::Unit::TestCase

    def test_validates_multiple_card_input
      assert_output(EXP_CARDS_STDOUT) { CLI.new.parse_options(CARDS) }
    end

    def test_validates_single_card_input
      assert_output(EXP_CARD_STDOUT) { CLI.new.parse_options(STDIN_CARD) }
    end
  end
end
