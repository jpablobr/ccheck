# -*- encoding: utf-8 -*-
require 'test_helper'
require 'minitest/autorun'

module CCheck
  class CCheckTest < MiniTest::Unit::TestCase

    def test_returns_the_correct_card_type
      assert_equal 'VISA'       , CCheck.type('4111111111111111')
      assert_equal 'VISA'       , CCheck.type('4111111111111')
      assert_equal 'VISA'       , CCheck.type('4012888888881881')
      assert_equal 'AMEX'       , CCheck.type('378282246310005')
      assert_equal 'Discover'   , CCheck.type('6011111111111117')
      assert_equal 'MasterCard' , CCheck.type('5105105105105100')
      assert_equal 'MasterCard' , CCheck.type('5105 1051 0510 5106')
      assert_equal 'Unknown'    , CCheck.type('9111111111111111')
    end

    def test_validates_if_card_is_valid_or_invalid
      assert CCheck.valid?('4111111111111111')    , 'Invalid card'
      refute CCheck.valid?('4111111111111')       , 'valid card'
      assert CCheck.valid?('4012888888881881')    , 'Invalid card'
      assert CCheck.valid?('378282246310005')     , 'Invalid card'
      assert CCheck.valid?('6011111111111117')    , 'Invalid card'
      assert CCheck.valid?('5105105105105100')    , 'Invalid card'
      refute CCheck.valid?('5105 1051 0510 5106') , 'valid card'
      refute CCheck.valid?('9111111111111111')    , 'valid card'
    end

    def test_print_method_can_accept_an_array_of_cards
      assert_output(EXP_CARDS_STDOUT) { CCheck.print(CARDS) }
    end
  end
end
